// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Cartograhpie de la Forge des communs numériques éducatifs',
  tagline: 'POC Bertrand',
  favicon: 'img/elea.ico',

  // Set the production url of your site here
  url: 'https://drane-grenoble.forge.apps.education.fr/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/poc-cartographie/',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'throw',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
         },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/elea.png',
      navbar: {
        title: '',
        logo: {
          alt: 'logo forge',
          src: 'img/elea.png',
        },
        items: [
          {type: 'docSidebar', sidebarId: 'SidebarApplications', position: 'left', label: 'Applications',},
          {type: 'docSidebar', sidebarId: 'SidebarRessources', position: 'left', label: 'Ressources',},
          {label: 'Mots Clés',href: 'https://drane-grenoble.forge.apps.education.fr/poc-cartographie/docs/tags/',position: 'left',},
        ],

      },
      footer: {
        style: 'dark',
        links: [
           {
            title: 'Communautés nationales',
            items: [
              {
                label: 'Une communauté',
                href: 'https://forge.apps.education.fr',
              },
              
            ],
          },
          {
            title: 'Communautés locales',
            items: [
              {
                label: 'Une communauté',
                href: 'https://forge.apps.education.fr',
              },
            ],
          },
        ],
        copyright: `Équipe de rédaction de la documentation de la Forge des Communs Numériques Éducatifs`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
    }),

  plugins: [
    [
      '@docusaurus/plugin-ideal-image',
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
        disableInDev: false,
      },
    ],
  ],
};

export default config;
