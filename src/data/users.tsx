/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* eslint-disable global-require */

import {translate} from '@docusaurus/Translate';
import {sortBy} from '@site/src/utils/jsUtils';

/*
 * ADD YOUR SITE TO THE DOCUSAURUS SHOWCASE
 *
 * Please don't submit a PR yourself: use the Github Discussion instead:
 * https://github.com/facebook/docusaurus/discussions/7826
 *
 * Instructions for maintainers:
 * - Add the site in the json array below
 * - `title` is the project's name (no need for the "Docs" suffix)
 * - A short (≤120 characters) description of the project
 * - Use relevant tags to categorize the site (read the tag descriptions on the
 *   https://docusaurus.io/showcase page and some further clarifications below)
 * - Add a local image preview (decent screenshot of the Docusaurus site)
 * - The image MUST be added to the GitHub repository, and use `require("img")`
 * - The image has to have minimum width 640 and an aspect of no wider than 2:1
 * - If a website is open-source, add a source link. The link should open
 *   to a directory containing the `docusaurus.config.js` file
 * - Resize images: node admin/scripts/resizeImage.js
 * - Run optimizt manually (see resize image script comment)
 * - Open a PR and check for reported CI errors
 *
 * Example PR: https://github.com/facebook/docusaurus/pull/7620
 */

// LIST OF AVAILABLE TAGS
// Available tags to assign to a showcase site
// Please choose all tags that you think might apply.
// We'll remove inappropriate tags, but it's less likely that we add tags.
export type TagType =
  | 'nouveautés'
  | 'école'
  | 'collège'
  | 'lycée'
  | 'application'
  | 'ressource';

// Add sites to this list
// prettier-ignore
const Users: User[] = [
  {
    title: 'Face Privacy',
    description: 'une application web qui permet de flouter facilement les visages',
    preview: require('/docs/Applications/img/faceprivacy_logo.png'),
    website: '/docs/Applications/face_privacy',
    tags: ['école', 'collège','lycée','application','nouveautés'],
  },
  {
    title: 'Ada et Zangemann',
    description: 'une application web qui permet de flouter facilement les visages',
    preview: require('/docs/Ressources/img/couverture_ada.jpg'),
    website: '/docs/Ressources/ada-le-livre',
    tags: ['école', 'ressource'],
  },
  {
    title: 'Codex',
    description: 'Site proposant des exercices d\'apprentissage de l\'algorithmique et de la programmation par le biais d\'exercices variés. Le langage utilisé est Python.',
    preview: require('/docs/Ressources/img/codex.png'),
    website: '/docs/Ressources/codex',
    tags: ['lycée', 'ressource'],
  },
  /*
  Pro Tip: add your site in alphabetical order.
  Appending your site here (at the end) is more likely to produce Git conflicts.
   */
];

export type User = {
  title: string;
  description: string;
  preview: string | null; // null = use our serverless screenshot service
  website: string;
  tags: TagType[];
};

export type Tag = {
  label: string;
  description: string;
  color: string;
};

export const Tags: {[type in TagType]: Tag} = {
  nouveautés: {
    label: translate({message: 'nouveautés'}),
    description: translate({
      message: 'Nouveaux dépots',
      id: 'showcase.tag.nouveau.description',
    }),
    color: '#e9669e',
  },

  école: {
    label: translate({message: 'école'}),
    description: translate({
      message: 'Pour les enseignants et élèves du premier degré',
      id: 'showcase.tag.ecole.description',
    }),
    color: '#39ca30',
  },

  collège: {
    label: translate({message: 'collège'}),
    description: translate({
      message: 'Pour les enseignants et élèves de collège',
      id: 'showcase.tag.college.description',
    }),
    color: '#dfd545',
  },

  lycée: {
    label: translate({message: 'lycée'}),
    description: translate({
      message: 'Pour les enseignants et élèves de lycée',
      id: 'showcase.tag.lycee.description',
    }),
    color: '#a44fb7',
  },

  application: {
    label: translate({message: 'application'}),
    description: translate({
      message: 'Une application en ligne ou à installer',
      id: 'showcase.tag.application.description',
    }),
    color: '#127f82',
  },

  ressource: {
    label: translate({message: 'Ressource'}),
    description: translate({
      message: 'Des ressources pédagogiques libres',
      id: 'showcase.tag.ressource.description',
    }),
    color: '#fe6829',
  },
};
export const TagList = Object.keys(Tags) as TagType[];
function sortUsers() {
  let result = Users;
  // Sort by site name
  result = sortBy(result, (user) => user.title.toLowerCase());
  // Sort by favorite tag, favorites first
  result = sortBy(result, (user) => !user.tags.includes('favorite'));
  return result;
}

export const sortedUsers = sortUsers();
